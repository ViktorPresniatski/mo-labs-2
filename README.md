# Machine Learning laboratory works (sem 2)

## Prerequisite: install jupyter lab
https://jupyter.org/install

## Create virtual environment
`python3 -m venv venv`

## Install requirements
`pip install -r requirements.txt`

## Run jupyter lab
`jupyter lab`
