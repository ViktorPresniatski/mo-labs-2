import os
import requests
import tarfile
import matplotlib.pyplot as plt
import numpy as np


def get_dataset_by_url(url):
    path = download_data(url)
    data_path = extract_data(path)
    X, y = read_data(data_path)
    return X, y


def download_data(url):
    filename = os.path.basename(url)
    dir_path = os.path.join(os.getcwd(), 'data')
    full_path = os.path.join(dir_path, filename)
    
    if os.path.exists(full_path):
        print("File already downloaded")
        return full_path

    os.makedirs(dir_path, exist_ok=True)
    r = requests.get(url)

    with open(full_path, 'wb') as f:
        f.write(r.content)

    return full_path


def extract_data(path):
    dir_name = os.path.basename(path).split(".")[0]
    dir_path = os.path.dirname(path)
    data_path = os.path.join(dir_path, dir_name)

    if os.path.exists(data_path):
        print("Data already extracted")
        return data_path

    with tarfile.open(path, "r") as tar:
        tar.extractall(path=dir_path)           

    return data_path


def read_data(path):
    X = []
    y = []
    
    dirs = sorted(filter(lambda d: not d.startswith('.'), os.listdir(path)))
    for i, d in enumerate(dirs):
        letter_dir = os.path.join(path, d)
        for filename in os.listdir(letter_dir):
            try:
                image = plt.imread(os.path.join(letter_dir, filename))
                image_vec = image.reshape(-1)
                X.append(image_vec)
                y.append(i)
            except Exception:
                pass

    return np.array(X), np.array(y)
